package com.societegenerale.kata

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

/**
 * Document Transaction
 */
@Document
data class Transaction(
        @Id val id: String? = null,
        val sum: BigDecimal,
        @CreatedDate val dateCreation: Date = Date(),
        val effectiveDate: Date,
        val type: TypeTransaction,
        val description: String? = null
)

/**
 * Enumeration Type transaction
 */
enum class TypeTransaction {
    DEPOSIT,WITHDRAWAL
}

/**
 * Handler
 */
@Component
class TransactionHandler(private val repository: TransactionRepository) {

    fun getAll(request: ServerRequest): Mono<ServerResponse> = ok().body(repository.findAll())

    fun add(request: ServerRequest): Mono<ServerResponse> {
        val transaction = request.bodyToMono<Transaction>()
        return ok().body(transaction.flatMap { repository.save(it)}.toMono())
    }

    fun total(request: ServerRequest): Mono<ServerResponse> {
        return ok().body(repository.findAll()
                .map { t:Transaction -> if (t.type == TypeTransaction.WITHDRAWAL) -t.sum else t.sum}
                .reduce { t: BigDecimal, u: BigDecimal ->  BigDecimal("${t+u}").setScale(2, RoundingMode.HALF_EVEN)}
                .switchIfEmpty(Mono.just(BigDecimal("0"))))
    }
}

/**
 * Repository
 */
@Repository
interface TransactionRepository: ReactiveCrudRepository<Transaction, String>
