package com.societegenerale.kata

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.router

@SpringBootApplication
class KataApplication

fun main(args: Array<String>) {
    runApplication<KataApplication>(*args)
}

@Bean
fun route() = router {
    accept(TEXT_HTML).nest {
        GET("/") { ok().render("index") }
    }
    resources("/**", ClassPathResource("public/"))
}
