package com.societegenerale.kata

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

@Configuration
class TransactionRouter(private val handler: TransactionHandler) {

    @Bean
    fun route() = router {
        ("/api/transaction" and accept(MediaType.APPLICATION_JSON)).nest {
            GET("/", handler::getAll)
            GET("/total", handler::total)
            POST("/", handler::add)
        }
    }
}
