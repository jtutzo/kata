package com.societegenerale.kata

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.*
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.util.*

@RunWith(SpringRunner::class)
@WebFluxTest
@Import(TransactionRouter::class, TransactionHandler::class)
class TransactionTests{

    @Autowired lateinit var client: WebTestClient

    @MockBean lateinit var transactionRepository: TransactionRepository

    private val emptyDatas: List<Transaction> = mutableListOf()

    private val datas: List<Transaction> = mutableListOf(
            Transaction("6", BigDecimal("0.25"), Date(), Date(), TypeTransaction.DEPOSIT, "Une description"),
            Transaction("7", BigDecimal("145"), Date(), Date(), TypeTransaction.WITHDRAWAL, "Une description"),
            Transaction("1", BigDecimal("125.36"), Date(), Date(), TypeTransaction.DEPOSIT, "Une description"),
            Transaction("2", BigDecimal("125.36"), Date(), Date(), TypeTransaction.DEPOSIT),
            Transaction("3", BigDecimal("45"), Date(), Date(), TypeTransaction.WITHDRAWAL, "Une description 3"),
            Transaction("4", BigDecimal("72.23"), Date(), Date(), TypeTransaction.WITHDRAWAL, "Une description 4"),
            Transaction("5", BigDecimal("0.12"), Date(), Date(), TypeTransaction.WITHDRAWAL, "Une description 5")
    )

    private val url: String = "api/transaction"

    @Test
    fun `Get all transaction when data is empty`() {
        given(transactionRepository.findAll()).willReturn(Flux.fromIterable(emptyDatas))
        client.get().uri(this.url).exchange()
                .expectStatus().isOk
                .expectBodyList(Transaction::class.java)
                .returnResult().apply { Assert.assertEquals(emptyDatas, responseBody) }
    }

    @Test
    fun `Get all transaction when data has many results`() {
        given(transactionRepository.findAll()).willReturn(Flux.fromIterable(datas))
        client.get().uri(this.url).exchange()
                .expectStatus().isOk
                .expectBodyList(Transaction::class.java)
                .returnResult().apply { Assert.assertEquals(datas, responseBody) }
    }

    @Test
    fun `Get total when data is empty`() {
        given(transactionRepository.findAll()).willReturn(Flux.fromIterable(emptyDatas))
        client.get().uri("${this.url}/total").exchange()
                .expectStatus().isOk
                .expectBody(BigDecimal::class.java)
                .returnResult().apply { Assert.assertEquals(BigDecimal("0"), responseBody) }
    }

    @Test
    fun `Get total when data has many results`() {
        given(transactionRepository.findAll()).willReturn(Flux.fromIterable(datas))
        client.get().uri("${this.url}/total").exchange()
                .expectStatus().isOk
                .expectBody(BigDecimal::class.java)
                .returnResult().apply { Assert.assertEquals(BigDecimal("-11.38"), responseBody) }
    }

    @Test
    fun `Test post transaction type deposit`() {
        val mono: Mono<Transaction> = Mono.just(datas.get(0))
        Mockito.`when`(transactionRepository.save(Mockito.any(Transaction::class.java))).thenReturn(mono)
        client.post().uri(this.url)
                .body(mono, Transaction::class.java).exchange()
                .expectStatus().isOk
                .expectBody(Transaction::class.java)
                .returnResult().apply { Assert.assertEquals(datas.get(0), responseBody) }
        verify(transactionRepository).save(datas.get(0))
    }

    @Test
    fun `Test post transaction type withdrawal`() {
        val mono: Mono<Transaction> = Mono.just(datas.get(1))
        Mockito.`when`(transactionRepository.save(Mockito.any(Transaction::class.java))).thenReturn(mono)
        client.post().uri(this.url)
                .body(mono, Transaction::class.java).exchange()
                .expectStatus().isOk
                .expectBody(Transaction::class.java)
                .returnResult().apply { Assert.assertEquals(datas.get(1), responseBody) }
        verify(transactionRepository).save(datas.get(1))
    }

}

