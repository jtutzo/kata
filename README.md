# Kata 
> Application démo pour la société générale

## Dépendances

 - Spring-boot 2.0.6.RELEASE
 - Docker 18.03.1-ce
 - Maven 3.5.4
 - npm 6.4.1
 - Java 8
 - Angular 6.1.0
 - Typescript : 3.13
 - Kotlin : 1.2.51
 - MongoDB : 4.0.3

## Architecture technique

```text
Kata
├─┬ backend     → Module maven backend avec Spring Boot
│ ├── src
│ └── pom.xml
├─┬ frontend    → Module maven frontend avec Angular
│ ├── src
│ └── pom.xml
└── pom.xml     → Pom parent maven
```

## Build

```bash
./mvn clean install
```

## Run
 
```bash
docker-compose up
```

## Test

Backend :
```bash
./mvn test -rf :backend
```

Frontend :
```bash
./npm run test
```
Accédez aux tests : [localhost:80](localhost:80)
