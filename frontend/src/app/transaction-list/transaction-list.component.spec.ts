import {RouterTestingModule} from '@angular/router/testing';
import {async, inject, TestBed} from "@angular/core/testing";
import {TransactionListComponent} from "./transaction-list.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmComponent} from "../confirm/confirm.component";
import {AlertComponent} from "../alert/alert.component";
import {TransactionTypePipe} from "../transaction.pipe";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Transaction, TransactionService, TransactionTypeEnum} from "../transaction.service";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../app.module";
import {HttpClient} from "@angular/common/http";

describe('TransactionListComponent', () => {
    let mockTransactions: Transaction[];
    let mockNumber: number;
    let component;
    let fixture;
    let compiled;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                NgbModule,
                HttpClientTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [
                TransactionListComponent,
                ConfirmComponent,
                AlertComponent,
                TransactionTypePipe
            ],
            providers: [TransactionService]
        }).compileComponents();
        mockTransactions = [
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "13", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.WITHDRAWAL, description: ""},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"}
        ];
        mockNumber = 1235.12;
        fixture = TestBed.createComponent(TransactionListComponent);
        component = fixture.debugElement.componentInstance;
        compiled = fixture.debugElement.nativeElement;
    }));

    it('should create the transaction list component', () => {
        expect(component).toBeTruthy();
    });

    it('Should load pagination', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            component.loadTransactionByPage().subscribe((result) => {
                let value = result;
                console.log(value.length)
                expect(value.length).toBe(5);
            });

            const req = httpMock.expectOne(transactionService.url);
            expect(req.request.method).toEqual('GET');
            req.flush(mockTransactions);
            httpMock.verify();
        })
    );

    it('Should load total', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            component.loadTotal().subscribe((result) => {
                let value = result;
                console.log(value);
                expect(value).toEqual(mockNumber);
            });

            const req = httpMock.expectOne(`${transactionService.url}/total`);
            expect(req.request.method).toEqual('GET');
            req.flush(mockNumber);
            httpMock.verify();
        })
    );

    it('should generate statement printing', () => {
        expect(() => component.generateStatementPrinting()).not.toThrow();
    });

    it('Should generate statmenent printing after init', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            component.loadTransactionByPage().subscribe(() => {
                expect(() => component.generateStatementPrinting()).not.toThrow();
            });

            const req = httpMock.expectOne(transactionService.url);
            expect(req.request.method).toEqual('GET');
            req.flush(mockTransactions);
            httpMock.verify();
        })
    );
});
