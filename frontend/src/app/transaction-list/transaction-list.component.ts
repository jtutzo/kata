import {Component, OnInit} from '@angular/core';
import {Transaction, TransactionService, TransactionTypeEnum} from "../transaction.service";
import {forkJoin, Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import * as Jspdf from 'jspdf';
import 'jspdf-autotable';
import {TranslateService} from '@ngx-translate/core';

const PAGE_SYZE = 5;

@Component({
    selector: 'app-transaction-list',
    template: `
        <app-alert type="success" [show]="showSuccess" (close)="showSuccess = false" delay="2000">
            {{'TransactionListComponent.MsgSuccess' | translate}}
        </app-alert>
        <app-alert type="danger" [show]="showError" (close)="showError = false">
            {{'TransactionListComponent.MsgError' | translate}}
        </app-alert>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-8">
                        {{'TransactionListComponent.Title' | translate}}
                    </div>
                    <div id="total" class="col-sm-4 d-flex flex-row-reverse">
                        {{'TransactionListComponent.Total' | translate}} : {{total}} €
                    </div>
                </div>
            </div>
            <div *ngIf="transactions && transactions.length > 0; else elseBlock" class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{'Column.Type' | translate}}</th>
                        <th scope="col">{{'Column.Sum' | translate}}</th>
                        <th scope="col">{{'Column.EffectiveDate' | translate}}</th>
                        <th scope="col">{{'Column.Description' | translate}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr *ngFor="let transaction of transactions">
                        <td scope="row">{{transaction.type | transactionType | async}}</td>
                        <td>{{transaction.sum | number: '.2'}}</td>
                        <td>{{transaction.effectiveDate | date: 'dd/MM/yyyy'}}</td>
                        <td>{{transaction.description}}</td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                <ngb-pagination class="d-flex justify-content-center" [collectionSize]="collectionSize"
                                [(page)]="currentPage"
                                [maxSize]="5" [rotate]="true" [ellipses]="false"
                                [boundaryLinks]="true" [pageSize]="pageSize"
                                (pageChange)="updatePagination()"></ngb-pagination>
            </div>
            <ng-template #elseBlock>
                <div class="card-body"><strong>{{'TransactionListComponent.NoTransaction' | translate}}</strong></div>
            </ng-template>
            <div class="card-footer card-footer d-flex flex-row-reverse">
                <button class="btn btn-outline-success" (click)="generateStatementPrinting()">
                    {{'Button.StatementPrinting' | translate}}
                </button>
            </div>
        </div>
    `
})
export class TransactionListComponent implements OnInit {

    private _transactions: Transaction[];

    transactions: Transaction[];
    total: number;

    currentPage: number = 1;
    collectionSize: number;
    pageSize: number = PAGE_SYZE;

    showSuccess: boolean;
    showError: boolean;

    constructor(private transactionService: TransactionService,
                private translate: TranslateService) {
    }

    ngOnInit(): void {
        forkJoin(
            this.loadTransactionByPage(),
            this.loadTotal()
        ).subscribe(
            () => this.showSuccess = true,
            () => this.showError = true
        )
    }

    updatePagination(): void {
        this.loadTransactionByPage().subscribe(
            () => this.showSuccess = true,
            () => this.showError = true
        );
    }

    generateStatementPrinting() {
        this.loadLabelsPdf().subscribe((labels) => {
            const total = `${labels['TransactionListComponent.Total']} : ${this.total}`;
            const columns = [labels['Column.Type'], labels['Column.Sum'], labels['Column.EffectiveDate'], labels['Column.Description']];
            const rows = [];

            (this._transactions?this._transactions:[]).forEach(transaction => {
                rows.push([
                    transaction.type == TransactionTypeEnum.DEPOSIT
                        ? labels['TransactionListComponent.Deposit']
                        : labels['TransactionListComponent.Withdrawal'],
                    transaction.sum,
                    new Date(transaction.effectiveDate).toLocaleDateString(
                        "en-US",
                        {
                            weekday: 'long',
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric'
                        }),
                    transaction.description ? transaction.description : ''])
            });

            this.generatePdf(labels['TransactionListComponent.StatementPrinting'], total, columns, rows);
        });
    }

    private loadTransactionByPage(): Observable<Transaction[]> {
        return (this._transactions ? of(this._transactions) : this.loadTransaction())
            .pipe(
                map((transactions) => {
                    return transactions.filter((value, index) =>
                        (this.currentPage - 1) * this.pageSize <= index && index < this.currentPage * this.pageSize)
                }),
                map(value => this.transactions = value)
            )
    }

    private loadTransaction(): Observable<Transaction[]> {
        return this.transactionService.getAll().pipe(
            map(transactions => {
                this.collectionSize = transactions.length;
                return this._transactions = transactions
            })
        )
    }

    private loadTotal(): Observable<number> {
        return this.transactionService.total().pipe(map(total => this.total = total));
    }

    private loadLabelsPdf() {
        return this.translate.get([
            'TransactionListComponent.StatementPrinting',
            'TransactionListComponent.Total',
            'Column.Type',
            'Column.Sum',
            'Column.EffectiveDate',
            'Column.Description',
            'TransactionListComponent.Deposit',
            'TransactionListComponent.Withdrawal'
        ]);
    }

    private generatePdf(title: string, total: string, columns: string[], rows: any[]) {
        const doc = new Jspdf('p', 'pt');
        const header = function (data) {
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            doc.text(title, data.settings.margin.left, 50);
            doc.setFontSize(12);
            doc.text(total, data.settings.margin.left, 70);
        };
        doc.autoTable(columns, rows, {
            addPageContent: header,
            margin: {top: 80},
        });

        doc.save("statement-printing.pdf");
    }

}
