import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TransactionListComponent} from "./transaction-list/transaction-list.component";
import {TransactionNewComponent} from "./transaction-new/transaction-new.component";
import {NotFoundComponent} from "./not-found/not-found.component";

export const routes: Routes = [
    {path: '', redirectTo: '/transaction', pathMatch: 'full'},
    {path: 'transaction', component: TransactionListComponent},
    {path: 'transaction/new', component: TransactionNewComponent},
    {path: '404', component: NotFoundComponent},
    {path: '**', redirectTo: '/404'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
