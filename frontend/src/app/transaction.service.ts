import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {catchError} from 'rxjs/operators';

const URL = "/api/transaction";

export interface Transaction {
    id?: string
    type: TransactionTypeEnum,
    sum: number,
    effectiveDate: Date,
    description?: string
}

export enum TransactionTypeEnum {
    DEPOSIT = 'DEPOSIT',
    WITHDRAWAL = 'WITHDRAWAL'
}

@Injectable()
export class TransactionService {

    public url: string = URL;

    constructor(private http: HttpClient) {
    }

    getAll(): Observable<Transaction[]> {
        return this.http
            .get<Transaction[]>(this.url)
            .pipe(catchError(this.handleError));
    }

    add(transaction: Transaction): Observable<Transaction> {
        return this.http
            .post<Transaction>(this.url, transaction)
            .pipe(catchError(this.handleError));
    }

    total(): Observable<number> {
        return this.http
            .get<number>(`${this.url}/total`)
            .pipe(catchError(this.handleError))
    }

    private handleError(res: HttpErrorResponse | any) {
        console.error(res.error || res.body.error);
        return observableThrowError(res.error || 'Server error');
    }
}
