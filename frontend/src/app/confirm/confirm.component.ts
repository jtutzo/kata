import {
    Component,
    EventEmitter,
    Input,
    Output,
    ViewChild
} from "@angular/core";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-confirm',
    template: `
        <ng-template #modalConfirm let-modal>
            <div class="modal-header">
                <ng-content select="[header]"></ng-content>
                <button type="button" class="close" aria-label="Close" (click)="modal.dismiss()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ng-content select="[body]"></ng-content>
            </div>
            <div class="modal-footer">
                <button class="btn btn-outline-secondary" (click)="modal.dismiss()">{{'Button.Reset' | translate}}
                </button>
                <button class="btn btn-outline-success" ngbAutofocus (click)="modal.close()">
                    {{'Button.Valide' | translate}}
                </button>
            </div>
        </ng-template>
    `
})
export class ConfirmComponent {

    @ViewChild('modalConfirm') modalConfirm;

    @Output() close: EventEmitter<void> = new EventEmitter();
    @Output() confirm: EventEmitter<void> = new EventEmitter();

    constructor(private modalService: NgbModal) {
    }

    @Input()
    set show(show: boolean) {
        if (show)
            this.openModal();
    }

    openModal() {
        setTimeout(() => {
            this.modalService.dismissAll();
            this.modalService.open(this.modalConfirm).result.then(
                () => this.confirm.emit(),
                () => this.close.emit()
            );
        });
    }

}
