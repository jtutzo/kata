import {RouterTestingModule} from '@angular/router/testing';
import {ConfirmComponent} from "./confirm.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {async, TestBed} from "@angular/core/testing";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../app.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('ConfirmComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NgbModule,
                RouterTestingModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [
                ConfirmComponent
            ]
        }).compileComponents();
    }));

    it('should create the confirm component', () => {
        const fixture = TestBed.createComponent(ConfirmComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

});
