import {Component} from '@angular/core';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    template: `
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" [href]="defaultUrl">{{'AppComponent.Title' | translate}}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul ngbDropdown class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a [ngClass]="{'nav-link dropdown-toggle': true, 'active': isActive('/transaction')}"
                           id="navbarDropdown"
                           ngbDropdownToggle>Transaction</a>
                        <div ngbDropdownMenu class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/transaction">{{'AppComponent.ItemAll' | translate}}</a>
                            <a class="dropdown-item" href="/transaction/new">{{'AppComponent.ItemNew' | translate}}</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <br/>
        <router-outlet></router-outlet>
        <footer></footer>
    `
})
export class AppComponent {

    defaultUrl: string = "/";

    constructor(location: Location, private translate: TranslateService) {
        translate.addLangs(["en", "fr"]);
        translate.setDefaultLang('en');

        let browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }

    isActive(url: string): boolean {
        return location.pathname.startsWith(url);
    }

}
