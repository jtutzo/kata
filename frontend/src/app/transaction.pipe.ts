import {Pipe, PipeTransform} from '@angular/core';
import {TransactionTypeEnum} from "./transaction.service";
import {TranslateService} from '@ngx-translate/core';
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";

@Pipe({name: 'transactionType'})
export class TransactionTypePipe implements PipeTransform{
    constructor(private translate: TranslateService) {}

    transform(value: TransactionTypeEnum, toLowerCase:boolean): Observable<string|null> {
        if (value && value.includes(TransactionTypeEnum.DEPOSIT)) {
            return this.translate.get("TransactionListComponent.Deposit").pipe(
                map(result => toLowerCase && result?result.toLowerCase():result),
                map((result) => value.replace(new RegExp(TransactionTypeEnum.DEPOSIT, 'g'), result))
            );
        }
        else if (value && value.includes(TransactionTypeEnum.WITHDRAWAL))
            return this.translate.get("TransactionListComponent.Withdrawal").pipe(
                map(result => toLowerCase && result?result.toLowerCase():result),
                map(result => value.replace(new RegExp(TransactionTypeEnum.WITHDRAWAL, 'g'), result))
            );
        return of(value);
    }
}
