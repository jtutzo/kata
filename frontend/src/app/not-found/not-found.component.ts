import {Component} from '@angular/core';

@Component({
    selector: 'app-not-found',
    template: `
        <div class="card">
            <div class="card-header">{{'NotFoundComponent.Title' | translate}}</div>
            <div class="card-body">
                <h5 class="card-title"><strong>{{'NotFoundComponent.TitleHeader' | translate}}</strong></h5>
                <p class="card-text">{{'NotFoundComponent.Msg' | translate}}</p>
            </div>
        </div>
    `
})
export class NotFoundComponent {}
