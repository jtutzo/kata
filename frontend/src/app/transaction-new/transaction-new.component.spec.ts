import {RouterTestingModule} from '@angular/router/testing';
import {async, inject, TestBed} from "@angular/core/testing";
import {TransactionNewComponent} from "./transaction-new.component";
import {NgbActiveModal, NgbDate, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {SelectModule} from 'ng-select';
import {TransactionService, TransactionTypeEnum} from "../transaction.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {ConfirmComponent} from "../confirm/confirm.component";
import {AlertComponent} from "../alert/alert.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../app.module";
import {HttpClient} from "@angular/common/http";
import {TransactionTypePipe} from "../transaction.pipe";

describe('TransactionNewComponent', () => {
    let component;
    let fixture;
    let compiled;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                NgbModule,
                SelectModule,
                HttpClientTestingModule,
                FormsModule,
                ReactiveFormsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [
                TransactionNewComponent,
                ConfirmComponent,
                AlertComponent,
                TransactionTypePipe
            ],
            providers: [NgbActiveModal, TransactionService]
        }).compileComponents();
        fixture = TestBed.createComponent(TransactionNewComponent);
        component = fixture.debugElement.componentInstance;
        compiled = fixture.debugElement.nativeElement;
    }));

    it('should create the transaction new component', () => {
        expect(component).toBeTruthy();
    });

    it('fields type, sum, effectiveDate should be required', () => {
        fixture.detectChanges();
        expect(component.transactionForm.valid).toBeFalsy();
        let type = component.transactionForm.controls['type'];
        let sum = component.transactionForm.controls['sum'];
        let effectiveDate = component.transactionForm.controls['effectiveDate'];
        [type, sum, effectiveDate].forEach((value) => {
            let errors = value.errors || {};
            expect(errors['required']).toBeTruthy();
        });
    });

    it('should be valide form', async(() => {
        fixture.detectChanges();

        component.transactionForm.controls['type'].setValue(TransactionTypeEnum.DEPOSIT);
        component.transactionForm.controls['sum'].setValue(145.2);
        component.transactionForm.controls['effectiveDate'].setValue(new NgbDate(1993, 10, 24));
        component.transactionForm.controls['description'].setValue("Une description");
        fixture.detectChanges();
        expect(component.transactionForm.valid).toBeTruthy();
        expect(compiled.querySelector('button[type="submit"]').disabled).toBeFalsy();

        component.transactionForm.controls['sum'].setValue(.22);
        fixture.detectChanges();
        expect(component.transactionForm.controls['sum'].valid).toBeTruthy();
        expect(compiled.querySelector('button[type="submit"]').disabled).toBeFalsy();
    }));

    it('should be invalide form', async(() => {
        fixture.detectChanges();

        component.transactionForm.controls['type'].setValue("DEPOSIT21");
        component.transactionForm.controls['sum'].setValue(12.569);
        fixture.detectChanges();
        ['type', 'sum'].forEach(value => {
            expect(component.transactionForm.controls[value].valid).toBeFalsy();
        });
        expect(compiled.querySelector('button[type="submit"]').disabled).toBeTruthy();

        component.transactionForm.controls['sum'].setValue("1A.56");
        fixture.detectChanges();
        expect(component.transactionForm.controls['sum'].valid).toBeFalsy();
        expect(compiled.querySelector('button[type="submit"]').disabled).toBeTruthy();

    }));

});
