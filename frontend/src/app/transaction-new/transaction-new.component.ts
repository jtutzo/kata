import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Transaction, TransactionService, TransactionTypeEnum} from "../transaction.service";
import {IOption} from 'ng-select';

@Component({
    selector: 'app-transaction-new',
    template: `
        <app-confirm [show]="showModal" (close)="showModal = false" (confirm)="save();">
            <h4 header>
                {{'TransactionNewComponent.TitleConfirm' | translate:paramConfirm | transactionType:true | async}}</h4>
            <div body>
                <strong>{{'TransactionNewComponent.MsgConfirm' | translate:paramConfirm | transactionType:true | async}}</strong>
                <p>{{'TransactionNewComponent.Msg2Confirm' | translate}}</p>
            </div>
        </app-confirm>

        <app-alert type="success" [show]="showSuccess" (close)="showSuccess = false" delay="2000">
            {{'TransactionNewComponent.MsgSuccess' | translate}}
        </app-alert>
        <app-alert type="danger" [show]="showError" (close)="showError = false">
            {{'TransactionNewComponent.MsgError' | translate}}
        </app-alert>

        <form [formGroup]="transactionForm" (ngSubmit)="onSubmit()">
            <div class="card">
                <div class="card-header">{{'TransactionNewComponent.Title' | translate}}</div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="type" class="col-sm-2 col-form-label">{{'Form.Type' | translate}}*</label>
                        <div class="col-sm-10">
                            <ng-select id="type" formControlName="type" required [options]="selectOptions">
                                <ng-template #optionTemplate let-option="option">
                                    {{option.label | transactionType | async}}
                                </ng-template>
                            </ng-select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sum" class="col-sm-2 col-form-label">{{'Form.Sum' | translate}}*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="sum" formControlName="sum" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date-effect"
                               class="col-sm-2 col-form-label">{{'Form.EffectiveDate' | translate}}*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="date-effect" ngbDatepicker #d="ngbDatepicker"
                                   (focus)="d.toggle()" formControlName="effectiveDate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description"
                               class="col-sm-2 col-form-label">{{'Form.Description' | translate}}</label>
                        <div class="col-sm-10">
                    <textarea class="form-control" id="description"
                              formControlName="description"></textarea>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <small>* {{'TransactionNewComponent.ObligatoryField' | translate}}</small>
                    </div>
                </div>
                <div class="card-footer d-flex flex-row-reverse">
                    <button type="submit" [disabled]="transactionForm.invalid" class="btn btn-outline-success">
                        {{'Button.Valide' | translate}}
                    </button>
                    <div class="p-1"></div>
                    <button type="reset" class="btn btn-outline-secondary" (click)="reset()">
                        {{'Button.Reset' | translate}}
                    </button>
                </div>
            </div>
        </form>
    `
})
export class TransactionNewComponent implements OnInit {

    transactionForm: FormGroup;
    paramConfirm;

    showSuccess: boolean;
    showError: boolean;
    showModal: boolean;

    selectOptions: Array<IOption>;

    constructor(
        private fb: FormBuilder,
        private transactionService: TransactionService) {
    }

    ngOnInit(): void {
        this.initForm();
        this.selectOptions = [
            {label: TransactionTypeEnum.DEPOSIT, value: TransactionTypeEnum.DEPOSIT},
            {label: TransactionTypeEnum.WITHDRAWAL, value: TransactionTypeEnum.WITHDRAWAL}
        ];
        this.paramConfirm = {type: "", sum: ""};
    }

    onSubmit(): void {
        this.paramConfirm = this.transactionForm.value;
        this.showModal = true;
    }

    save() {
        this.showModal = false;
        const transaction = this.getForm();
        this.transactionService.add(transaction).subscribe(
            () => (this.showSuccess = true),
            () => (this.showError = true)
        );
    }

    reset(): void {
        this.transactionForm.reset();
    }

    private getForm(): Transaction {
        const transaction = this.transactionForm.value;
        const effectiveDate = this.transactionForm.controls['effectiveDate'].value;
        transaction['effectiveDate'] = new Date(effectiveDate.year, effectiveDate.month - 1, effectiveDate.day);
        return transaction;
    }

    private initForm() {
        this.transactionForm = this.fb.group({
            type: [null, [Validators.required, Validators.pattern(`^(${TransactionTypeEnum.DEPOSIT}|${TransactionTypeEnum.WITHDRAWAL})$`)]],
            sum: [null, [Validators.required, Validators.pattern('^[0-9]*(\\.[0-9]{1,2}){0,1}$')]],
            effectiveDate: [null, Validators.required],
            description: null
        });
    }

}
