import {inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Transaction, TransactionService, TransactionTypeEnum} from "./transaction.service";

describe('TransactionService', () => {
    let mockTransactions: Transaction[];
    let mockNumber: number;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [TransactionService]
        });
        mockTransactions = [
            {id: "12", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.DEPOSIT, description: "Coucou"},
            {id: "13", sum: 12.56, effectiveDate: new Date(), type: TransactionTypeEnum.WITHDRAWAL, description: ""}
        ];
        mockNumber = 1235.12;
    });

    it('should get all transactions is empty', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            transactionService.getAll().subscribe(transactions => {
                expect(transactions.length).toBe(0);
                expect(transactions.entries).toEqual([].entries);
            });
            const req = httpMock.expectOne(transactionService.url);
            expect(req.request.method).toEqual('GET');

            req.flush([]);
            httpMock.verify();
        })
    );

    it('should get all transactions', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            transactionService.getAll().subscribe(transactions => {
                expect(transactions.length).toBe(2);
                expect(transactions[0].id).toEqual("12");
                expect(transactions[1].id).toEqual("13");
                expect(transactions.entries).toEqual(mockTransactions.entries);
            });
            const req = httpMock.expectOne(transactionService.url);
            expect(req.request.method).toEqual('GET');

            req.flush(mockTransactions);
            httpMock.verify();
        })
    );

    it('Should add transaction', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            transactionService.add(mockTransactions[0]).subscribe(transaction => {
                expect(transaction.valueOf).toBe(mockTransactions[0].valueOf);
            });
            const req = httpMock.expectOne(`${transactionService.url}`);
            expect(req.request.method).toEqual('POST');

            req.flush(mockTransactions[0]);
            httpMock.verify();
        })
    );

    it('Should get total is 0', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            transactionService.total().subscribe(total => {
                let result = total;
                expect(result).toBe(0);
            });
            const req = httpMock.expectOne(`${transactionService.url}/total`);
            expect(req.request.method).toEqual('GET');

            req.flush(0);
            httpMock.verify();
        })
    );

    it('Should get total is 1235.12', inject([HttpTestingController, TransactionService],
        (httpMock: HttpTestingController, transactionService: TransactionService) => {
            transactionService.total().subscribe(total => {
                let result = total;
                expect(result).toBe(mockNumber);
            });
            const req = httpMock.expectOne(`${transactionService.url}/total`);
            expect(req.request.method).toEqual('GET');

            req.flush(mockNumber);
            httpMock.verify();
        })
    );
});
