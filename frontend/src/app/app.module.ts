import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from "./app-routing.module";
import {ReactiveFormsModule} from '@angular/forms';
import {SelectModule} from 'ng-select';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {AppComponent} from './app.component';
import {NotFoundComponent} from "./not-found/not-found.component";
import {TransactionListComponent} from "./transaction-list/transaction-list.component";
import {TransactionNewComponent} from "./transaction-new/transaction-new.component";
import {TransactionService} from "./transaction.service";
import {AlertComponent} from "./alert/alert.component";
import {TransactionTypePipe} from "./transaction.pipe";
import {ConfirmComponent} from "./confirm/confirm.component";

@NgModule({
    declarations: [
        AppComponent,
        NotFoundComponent,
        TransactionListComponent,
        TransactionNewComponent,
        AlertComponent,
        TransactionTypePipe,
        ConfirmComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        SelectModule,
        CommonModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [TransactionService, NgbActiveModal],
    bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
