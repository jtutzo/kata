import {TestBed, async, fakeAsync, tick} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {routes} from "./app-routing.module";
import {Router} from "@angular/router";
import {TransactionListComponent} from "./transaction-list/transaction-list.component";
import {TransactionNewComponent} from "./transaction-new/transaction-new.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {AlertComponent} from "./alert/alert.component";
import {ConfirmComponent} from "./confirm/confirm.component";
import {TransactionTypePipe} from "./transaction.pipe";
import {NgbActiveModal, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {SelectModule} from 'ng-select';
import {Location} from "@angular/common";
import {TransactionService} from "./transaction.service";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "./app.module";

describe('AppComponent', () => {
    let router;
    let location;
    let fixture;
    let component;
    let compiled;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                NgbModule,
                FormsModule,
                HttpClientModule,
                ReactiveFormsModule,
                SelectModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [
                AppComponent,
                TransactionListComponent,
                TransactionNewComponent,
                NotFoundComponent,
                AlertComponent,
                ConfirmComponent,
                TransactionTypePipe
            ],
            providers: [TransactionService, NgbActiveModal],
        }).compileComponents();

        router = TestBed.get(Router);
        location = TestBed.get(Location);

        fixture = TestBed.createComponent(AppComponent);
        component = fixture.debugElement.componentInstance;
        compiled = fixture.debugElement.nativeElement;
        router.initialNavigation();
    }));

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it(`should have as title 'kata'`, () => {
        fixture.detectChanges();
        expect(compiled.querySelector('a.navbar-brand').textContent).toEqual('Kata');
    });

    it('navigate to "" redirects you to /transaction', fakeAsync(() => {
        router.navigate(['']);
        tick();
        expect(location.path()).toBe('/transaction');
    }));

    it('navigate to "transaction" takes you to /transaction', fakeAsync(() => {
        router.navigate(['transaction']);
        tick();
        expect(location.path()).toBe('/transaction');
    }));

    it('navigate to "transaction/new" takes you to /transaction/new', fakeAsync(() => {
        router.navigate(['transaction/new']);
        tick();
        expect(location.path()).toBe('/transaction/new');
    }));

    it('navigate to "coucou" takes you to /404', fakeAsync(() => {
        router.navigate(['coucou']);
        tick();
        expect(location.path()).toBe('/404');
    }));
});
