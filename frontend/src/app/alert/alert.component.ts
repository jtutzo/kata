import {Component, EventEmitter, Input, Output} from '@angular/core';
import {timer} from 'rxjs';

@Component({
    selector: 'app-alert',
    template: `
        <ngb-alert [type]="type" *ngIf="showAlert" (close)="close.emit()"><ng-content></ng-content></ngb-alert>
    `
})
export class AlertComponent {

    private _timer;
    showAlert: boolean;

    @Input() type: string;
    @Input() delay: number;
    @Output() close: EventEmitter<void> = new EventEmitter<void>();

    @Input()
    set show(show: boolean) {
        this.stopTimer();
        if (show && this.delay && this.delay > 0)
            this.starTimer(this.delay);
        this.showAlert = show;
    }

    private starTimer(delay: number): void {
        this._timer = timer(delay).subscribe(() => this.close.emit());
    }

    private stopTimer(): void {
        if (this._timer) this._timer.unsubscribe();
    }

}
